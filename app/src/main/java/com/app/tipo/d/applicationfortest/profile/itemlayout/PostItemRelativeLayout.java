package com.app.tipo.d.applicationfortest.profile.itemlayout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class PostItemRelativeLayout extends RelativeLayout {
    public PostItemRelativeLayout(Context context) {
        super(context);
    }

    public PostItemRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PostItemRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PostItemRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
