package com.app.tipo.d.applicationfortest.profile;

import android.content.Context;

import com.app.tipo.d.applicationfortest.api.ApiData;
import com.app.tipo.d.applicationfortest.api.ApiInterface;
import com.app.tipo.d.applicationfortest.api.ServiceGenerator;
import com.app.tipo.d.applicationfortest.profile.adapter.PostItem;
import com.app.tipo.d.applicationfortest.post.data.Pagination;
import com.app.tipo.d.applicationfortest.post.data.Post;
import com.app.tipo.d.applicationfortest.post.data.PostsRequestData;
import com.app.tipo.d.applicationfortest.profile.data.user.ItselfUser;
import com.app.tipo.d.applicationfortest.profile.data.user.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter implements ProfileContract.Presenter {
    ProfileContract.View userInformationView;
    Pagination postsPagination;

    public ProfilePresenter(ProfileContract.View userInformationView) {
        this.userInformationView = userInformationView;
    }

    @Override
    public User getItselfUser() {
        return ItselfUser.getInstance();
    }

    @Override
    public void loadPostsFirstPage(int count) {
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        final ApiData apiData = ApiData.getInstance();
        final String accessToken = apiData.getAccessToken();
        Call<PostsRequestData> call = apiInterface.getItselfMedia(accessToken, count, null);

        call.enqueue(new PostsLoadingCallback());
    }

    @Override
    public void loadPostsNextPage(int count) {
        String maxId = retrieveMaxId();
        if (maxId != null) {
            ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
            final ApiData apiData = ApiData.getInstance();
            final String accessToken = apiData.getAccessToken();
            Call<PostsRequestData> call = apiInterface.getItselfMedia(accessToken, count, maxId);

            call.enqueue(new PostsLoadingCallback());
        } else {
            userInformationView.showPosts(null);
        }
    }

    private String retrieveMaxId() {
        String maxId = null;
        if (postsPagination != null) {
            maxId = postsPagination.getNextMaxId();
        }
        return maxId;
    }

    @Override
    public List<PostItem> preparePostsItems(List<Post> posts, Context context) {
        List<PostItem> postsItems = null;

        if (posts != null) {
            postsItems = new ArrayList<>(posts.size());
            for (int postPosition = 0; postPosition < posts.size(); ++postPosition) {
                postsItems.add(new PostItem(posts.get(postPosition), context));
            }
        }

        return postsItems;
    }

    private class PostsLoadingCallback implements Callback<PostsRequestData> {
        @Override
        public void onResponse(Call<PostsRequestData> call, Response<PostsRequestData> response) {
            ApiData apiData = ApiData.getInstance();

            if (response.isSuccessful()) {
                PostsRequestData postsRequestData = response.body();
                if (postsRequestData.getMeta().getCode() == apiData.getSuccessCode()) {
                    postsPagination = postsRequestData.getPagination();
                    List<Post> posts = postsRequestData.getPosts();
                    userInformationView.showPosts(posts);
                } else {
                    userInformationView.showPosts(null);
                }
            } else {
                userInformationView.showPosts(null);
            }
        }

        @Override
        public void onFailure(Call<PostsRequestData> call, Throwable t) {
            userInformationView.showPosts(null);
        }
    }
}
