package com.app.tipo.d.applicationfortest.images.processing;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

import com.squareup.picasso.Transformation;

public class BlurTransformation implements Transformation {
    RenderScript renderScript;

    public BlurTransformation(Context context) {
        super();
        renderScript = RenderScript.create(context);
    }

    @Override
    public Bitmap transform(Bitmap source) {
        Bitmap blurredBitmap = source.copy(Bitmap.Config.ARGB_8888, true);

        Allocation input = Allocation.createFromBitmap(
                renderScript, blurredBitmap,
                Allocation.MipmapControl.MIPMAP_FULL, Allocation.USAGE_SHARED);
        Allocation output = Allocation.createTyped(renderScript, input.getType());

        ScriptIntrinsicBlur script
                = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));

        script.setInput(input);
        script.setRadius(25);
        script.forEach(output);
        output.copyTo(blurredBitmap);

        source.recycle();

        return blurredBitmap;
    }

    @Override
    public String key() {
        return "blur";
    }
}
