package com.app.tipo.d.applicationfortest.login;

import android.support.annotation.StringRes;

import com.app.tipo.d.applicationfortest.BasePresenter;
import com.app.tipo.d.applicationfortest.BaseView;

public interface LoginContract {
    interface View extends BaseView<Presenter> {
        void startNextActivity();
        void showLoadingStarted(boolean loadingStarted);
        void notifyLoadingError(@StringRes int errorMessageId);
    }

    interface Presenter extends BasePresenter {
        boolean retrieveAccessToken(String url);
        void loadItself();
    }
}
