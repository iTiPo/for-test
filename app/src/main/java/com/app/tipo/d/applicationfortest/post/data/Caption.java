package com.app.tipo.d.applicationfortest.post.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Caption implements Parcelable {
    public static final Creator<Caption> CREATOR = new Creator<Caption>() {
        @Override
        public Caption createFromParcel(Parcel source) {
            return new Caption(source);
        }

        @Override
        public Caption[] newArray(int size) {
            return new Caption[size];
        }
    };

    @SerializedName("text")
    private String text;

    public Caption(Parcel source) {
        this.text = source.readString();
    }

    public String getText() {
        return text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
    }
}
