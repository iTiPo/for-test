package com.app.tipo.d.applicationfortest.post.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostsRequestData {
    @SerializedName("pagination")
    private Pagination pagination;

    @SerializedName("meta")
    private Meta meta;

    @SerializedName("data")
    private List<Post> posts;

    public Pagination getPagination() {
        return pagination;
    }

    public Meta getMeta() {
        return meta;
    }

    public List<Post> getPosts() {
        return posts;
    }
}
