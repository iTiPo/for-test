package com.app.tipo.d.applicationfortest.login;

import com.app.tipo.d.applicationfortest.R;
import com.app.tipo.d.applicationfortest.api.ApiData;
import com.app.tipo.d.applicationfortest.api.ApiInterface;
import com.app.tipo.d.applicationfortest.api.ServiceGenerator;
import com.app.tipo.d.applicationfortest.profile.data.user.ItselfUser;
import com.app.tipo.d.applicationfortest.profile.data.user.UserRequestData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements LoginContract.Presenter {
    ApiData apiData;
    LoginContract.View loginView;

    public LoginPresenter(LoginContract.View loginView, ApiData apiData) {
        this.apiData = apiData;
        this.loginView = loginView;
    }

    @Override
    public boolean retrieveAccessToken(String url) {
        if (url.indexOf(apiData.getRedirectUri()) == 0) {
            String accessToken = url.substring(url.indexOf('=') + 1);
            apiData.setAccessToken(accessToken);
            loadItself();
            return true;
        }
        return false;
    }

    @Override
    public void loadItself() {
        loginView.showLoadingStarted(true);
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<UserRequestData> call = apiInterface.getItself(apiData.getAccessToken());

        call.enqueue(new Callback<UserRequestData>() {
            @Override
            public void onResponse(Call<UserRequestData> call, Response<UserRequestData> response) {
                loginView.showLoadingStarted(false);
                if (response.isSuccessful()) {
                    UserRequestData userRequestData = response.body();
                    if (userRequestData.getMeta().getCode() == apiData.getSuccessCode()) {
                        ItselfUser.getInstance().setFromSource(userRequestData.getUser());
                        loginView.startNextActivity();
                    } else {
                        loginView.notifyLoadingError(R.string.api_error);
                    }
                } else {
                    loginView.notifyLoadingError(R.string.api_error);
                }
            }

            @Override
            public void onFailure(Call<UserRequestData> call, Throwable t) {
                loginView.showLoadingStarted(false);
                loginView.notifyLoadingError(R.string.connection_problem);
            }
        });
    }
}
