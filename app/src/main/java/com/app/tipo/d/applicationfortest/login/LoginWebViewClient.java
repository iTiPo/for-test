package com.app.tipo.d.applicationfortest.login;

import android.graphics.Bitmap;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LoginWebViewClient extends WebViewClient {
    LoginContract.Presenter presenter;

    public void setPresenter(LoginContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (!presenter.retrieveAccessToken(url)) {
            view.loadUrl(url);
        }
        return true;
    }

    /*@Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        presenter.retrieveAccessToken(url);
    }*/
}
