package com.app.tipo.d.applicationfortest.profile.data.user;

public class ItselfUser extends User {
    private static ItselfUser instance;

    private ItselfUser() {
    }

    public static ItselfUser getInstance() {
        if (instance == null) {
            instance = new ItselfUser();
        }
        return instance;
    }

    public boolean namesContainQuery(String query) {
        String upperQuery = query.toUpperCase();
        return containFactorIsCompleted(upperQuery.length())
                && (name != null && name.toUpperCase().contains(upperQuery)
                    || fullName != null && fullName.toUpperCase().contains(upperQuery));
    }

    private boolean containFactorIsCompleted(int queryLength) {
        final double CONTAIN_FACTOR = 0.6;
        return (double) queryLength / (double) name.length() >= CONTAIN_FACTOR
                || (double) queryLength / (double) fullName.length() >= CONTAIN_FACTOR;
    }
}
