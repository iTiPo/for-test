package com.app.tipo.d.applicationfortest.search;

import android.content.Context;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.app.tipo.d.applicationfortest.api.ApiData;
import com.app.tipo.d.applicationfortest.search.adapter.UsersSearchItem;
import com.app.tipo.d.applicationfortest.profile.data.user.ItselfUser;

import java.util.ArrayList;
import java.util.List;

public class SearchPresenter implements SearchContract.Presenter {
    private SearchContract.View searchView;

    public SearchPresenter(SearchContract.View searchView) {
        this.searchView = searchView;
    }

    @Override
    public void searchUsers(String query) {
        List<UsersSearchItem> users = new ArrayList<>();
        ItselfUser itselfUser = ItselfUser.getInstance();
        if (query.length() > 0 && itselfUser.namesContainQuery(query)) {
            users.add(new UsersSearchItem(itselfUser.getName()));
        }
        searchView.showUsers(users);
    }

    @Override
    public void logout(Context context) {
        ApiData.getInstance().setAccessToken(null);
        clearCookies(context);
    }

    private void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else
        {
            CookieSyncManager cookieSyncMngr= CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }
}
