package com.app.tipo.d.applicationfortest.search;

import android.content.Context;

import com.app.tipo.d.applicationfortest.BasePresenter;
import com.app.tipo.d.applicationfortest.BaseView;
import com.app.tipo.d.applicationfortest.search.adapter.UsersSearchItem;

import java.util.List;

public interface SearchContract {
    interface View extends BaseView<Presenter> {
        void showUsers(List<UsersSearchItem> users);
        void startLoginActivity();
    }

    interface Presenter extends BasePresenter {
        void searchUsers(String query);
        void logout(Context context);
    }
}
