package com.app.tipo.d.applicationfortest.login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.tipo.d.applicationfortest.R;
import com.app.tipo.d.applicationfortest.api.ApiData;
import com.app.tipo.d.applicationfortest.search.SearchActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {
    private LoginContract.Presenter loginPresenter;

    @BindView(R.id.login_web_view)
    WebView loginWebView;
    @BindView(R.id.error_container)
    LinearLayout errorContainer;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.loading_progress_bar)
    ProgressBar loadingProgressBar;

    public static Intent createActivityIntent(Context packageContext) {
        return new Intent(packageContext, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setPresenter(new LoginPresenter(this, ApiData.getInstance()));
        setupLoginWebView();
    }

    private void setupLoginWebView() {
        LoginWebViewClient loginWebViewClient = new LoginWebViewClient();
        loginWebViewClient.setPresenter(loginPresenter);
        loginWebView.setWebViewClient(loginWebViewClient);
        loginWebView.getSettings().setJavaScriptEnabled(true);
        loginWebView.loadUrl(buildLoginUrl());
    }

    private String buildLoginUrl() {
        ApiData apiData = ApiData.getInstance();
        return String.format(apiData.getLoginUrlFormat(), apiData.getClientId(), apiData.getRedirectUri());
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        loginPresenter = presenter;
    }

    @Override
    public void startNextActivity() {
        Intent intent = SearchActivity.createActivityIntent(getApplicationContext());
        startActivity(intent);
        finish();
    }

    @Override
    public void notifyLoadingError(@StringRes int errorMessageId) {
        setErrorContainerVisibility(View.VISIBLE);
        setErrorMessage(errorMessageId);
    }

    private void setErrorContainerVisibility(int visibility) {
        errorContainer.setVisibility(visibility);
    }

    private void setErrorMessage(@StringRes int errorMessageId) {
        errorMessage.setText(errorMessageId);
    }

    @Override
    public void showLoadingStarted(boolean loadingStarted) {
        if (loadingStarted) {
            loadingProgressBar.setVisibility(View.VISIBLE);
        } else {
            loadingProgressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.retry_button)
    public void onRetryLoadItself(View view) {
        setErrorContainerVisibility(View.GONE);
        loginPresenter.loadItself();
    }
}
