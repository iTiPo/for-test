package com.app.tipo.d.applicationfortest.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.tipo.d.applicationfortest.R;
import com.app.tipo.d.applicationfortest.post.PostActivity;
import com.app.tipo.d.applicationfortest.profile.adapter.PostItem;
import com.app.tipo.d.applicationfortest.post.data.Post;
import com.app.tipo.d.applicationfortest.profile.data.user.User;
import com.app.tipo.d.applicationfortest.profile.data.user.UserCountsInformation;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.items.ProgressItem;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements ProfileContract.View {
    private final String COUNT_DESCRIPTION_FORMAT = "%d\n%s";
    private final int SPAN_COUNT = 3;

    private ProfileContract.Presenter presenter;
    private User user;
    private FastItemAdapter<PostItem> fastItemAdapter;
    private FooterAdapter<ProgressItem> footerAdapter;

    @BindView(R.id.user_avatar)
    ImageView userAvatar;
    @BindView(R.id.user_fullname)
    TextView userFullname;
    @BindView(R.id.user_username)
    TextView userUsername;
    @BindView(R.id.user_bio)
    TextView userBio;
    @BindView(R.id.user_website)
    TextView userWebsite;
    @BindView(R.id.user_media_count)
    TextView userMediaCount;
    @BindView(R.id.user_followed_by_count)
    TextView userFollowedByCount;
    @BindView(R.id.user_follows_count)
    TextView userFollowsCount;
    @BindView(R.id.user_posts_recycler)
    RecyclerView userPostsRecycler;

    public static Intent createActivityIntent(Context packageContext) {
        return new Intent(packageContext, ProfileActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setPresenter(new ProfilePresenter(this));
        setupUserInformation();
        setupRecyclerView();
        presenter.loadPostsFirstPage(6);
    }

    @Override
    public void setPresenter(ProfileContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private void setupUserInformation() {
        user = presenter.getItselfUser();
        setupImage();
        setupInformationPart(userFullname, user.getFullName());
        setupInformationPart(userUsername, user.getName());
        setupInformationPart(userBio, user.getBio());
        setupInformationPart(userWebsite, user.getWebsite());

        UserCountsInformation counts = user.getCounts();
        setupCountsInformation(userMediaCount, counts.getMediaCount(), R.string.media_count_title);
        setupCountsInformation(userFollowedByCount, counts.getFollowedByCount(), R.string.followed_by_count_title);
        setupCountsInformation(userFollowsCount, counts.getFollowersCount(), R.string.follows_count_title);
    }

    private void setupImage() {
        Picasso.with(this)
                .load(user.getProfilePictureUrl())
                .config(Bitmap.Config.RGB_565)
                .fit()
                .centerInside()
                .into(userAvatar);
    }

    private void setupInformationPart(TextView information, String text) {
        if (text != null) {
            information.setText(text);
        } else {
            setViewVisibility(information, View.GONE);
        }
    }

    private void setViewVisibility(View view, int visibility) {
        view.setVisibility(visibility);
    }

    private void setupCountsInformation(TextView countInformation, int count,
                                        @StringRes int countTitleId) {
        String countTitle = getString(countTitleId);
        String countDescription = String.format(COUNT_DESCRIPTION_FORMAT, count, countTitle);
        countInformation.setText(countDescription);
    }

    private void setupRecyclerView() {
        setupAdapter();
        userPostsRecycler.setLayoutManager(new GridLayoutManager(this, SPAN_COUNT));
        userPostsRecycler.getLayoutManager();
        footerAdapter = new FooterAdapter<>();
        userPostsRecycler.setAdapter(footerAdapter.wrap(fastItemAdapter));
        userPostsRecycler.addOnScrollListener(new EndlessRecyclerOnScrollListener(footerAdapter) {
            @Override
            public void onLoadMore(int currentPage) {
                footerAdapter.clear();
                footerAdapter.add(new ProgressItem().withEnabled(false));
                presenter.loadPostsNextPage(6);
            }
        });
    }

    private void setupAdapter() {
        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<PostItem>() {
            @Override
            public boolean onClick(View v, IAdapter<PostItem> adapter, PostItem item, int position) {
                startPostActivity(item.getPost());
                return true;
            }
        });
    }

    private void startPostActivity(Post post) {
        Intent intent = PostActivity.createActivityIntent(this, post);
        startActivity(intent);
    }

    @Override
    public void showPosts(List<Post> postItems) {
        if (postItems != null) {
            List<PostItem> postsItems = presenter.preparePostsItems(postItems, getApplicationContext());
            if (postsItems != null) {
                fastItemAdapter.add(postsItems);
            }
        }
        footerAdapter.clear();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState = fastItemAdapter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);
    }
}
