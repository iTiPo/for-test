package com.app.tipo.d.applicationfortest.profile.data.user;

import com.google.gson.annotations.SerializedName;

public class UserRequestData {
    @SerializedName("meta")
    private RequestMeta meta;
    @SerializedName("data")
    private User user;

    public RequestMeta getMeta() {
        return meta;
    }

    public User getUser() {
        return user;
    }
}
