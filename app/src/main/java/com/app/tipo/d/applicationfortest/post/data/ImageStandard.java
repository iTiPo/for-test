package com.app.tipo.d.applicationfortest.post.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ImageStandard implements Parcelable {
    public static final Creator<ImageStandard> CREATOR = new Creator<ImageStandard>() {
        @Override
        public ImageStandard createFromParcel(Parcel source) {
            return new ImageStandard(source);
        }

        @Override
        public ImageStandard[] newArray(int size) {
            return new ImageStandard[size];
        }
    };

    @SerializedName("url")
    private String url;

    public ImageStandard(Parcel source) {
        this.url = source.readString();
    }

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
    }
}
