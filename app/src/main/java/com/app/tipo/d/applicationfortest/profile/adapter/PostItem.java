package com.app.tipo.d.applicationfortest.profile.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.app.tipo.d.applicationfortest.R;
import com.app.tipo.d.applicationfortest.images.processing.BlurTransformation;
import com.app.tipo.d.applicationfortest.post.data.Post;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;

public class PostItem extends AbstractItem<PostItem, PostItem.ViewHolder> {
    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();
    private static final String VIDEO_TYPE = "video";

    private Post post;
    private Context context;

    public PostItem(Post post, Context context) {
        this.post = post;
        this.context = context;
    }

    public Post getPost() {
        return post;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.post_item;
    }

    @Override
    public int getType() {
        return R.id.adapter_posts_item_id;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        loadThumbnail(holder.postPreviewImage);
        setupVideoIcon(holder.videoIcon);
    }

    private void loadThumbnail(final ImageView imageView) {
        Picasso.with(context)
                .load(post.getImages().getThumbnail().getUrl())
                .transform(new BlurTransformation(context))
                .config(Bitmap.Config.RGB_565)
                .fit()
                .centerInside()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        loadOriginal(imageView);
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    private void loadOriginal(ImageView imageView) {
        Picasso.with(context)
                .load(post.getImages().getStandard().getUrl())
                .noPlaceholder()
                .config(Bitmap.Config.RGB_565)
                .fit()
                .centerInside()
                .into(imageView);
    }

    private void setupVideoIcon(ImageView videoIcon) {
        if (post.getType().equals(VIDEO_TYPE)) {
            videoIcon.setVisibility(View.VISIBLE);
        } else {
            videoIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public ViewHolderFactory<? extends ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView postPreviewImage;
        ImageView videoIcon;

        public ViewHolder(View view) {
            super(view);
            postPreviewImage = ButterKnife.findById(view, R.id.post_preview_image);
            videoIcon = ButterKnife.findById(view, R.id.video_icon);
        }
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }
}
