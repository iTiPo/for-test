package com.app.tipo.d.applicationfortest.post.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Post implements Parcelable {
    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    @SerializedName("type")
    private String type;

    @SerializedName("comments")
    private Comments comments;

    @SerializedName("likes")
    private Likes likes;

    @SerializedName("created_time")
    private int creationTime;

    @SerializedName("images")
    private Images images;

    @SerializedName("caption")
    private Caption caption;

    public Post(Parcel source) {
        this.type = source.readString();
        this.comments = source.readParcelable(Comments.class.getClassLoader());
        this.likes = source.readParcelable(Likes.class.getClassLoader());
        this.creationTime = source.readInt();
        this.images = source.readParcelable(Images.class.getClassLoader());
        this.caption = source.readParcelable(Caption.class.getClassLoader());
    }

    public String getType() {
        return type;
    }

    public Comments getComments() {
        return comments;
    }

    public Likes getLikes() {
        return likes;
    }

    public int getCreationTime() {
        return creationTime;
    }

    public Images getImages() {
        return images;
    }

    public Caption getCaption() {
        return caption;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeParcelable(comments, flags);
        dest.writeParcelable(likes, flags);
        dest.writeInt(creationTime);
        dest.writeParcelable(images, flags);
        dest.writeParcelable(caption, flags);
    }
}
