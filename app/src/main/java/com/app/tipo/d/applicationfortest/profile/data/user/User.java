package com.app.tipo.d.applicationfortest.profile.data.user;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("username")
    protected String name;
    @SerializedName("bio")
    protected String bio;
    @SerializedName("website")
    protected String website;
    @SerializedName("profile_picture")
    protected String profilePictureUrl;
    @SerializedName("full_name")
    protected String fullName;
    @SerializedName("counts")
    protected UserCountsInformation counts;

    public String getName() {
        return name;
    }

    public String getBio() {
        return bio;
    }

    public String getWebsite() {
        return website;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public UserCountsInformation getCounts() {
        return counts;
    }

    public void setFromSource(User sourceUser) {
        this.name = sourceUser.name;
        this.bio = sourceUser.bio;
        this.website = sourceUser.website;
        this.profilePictureUrl = sourceUser.profilePictureUrl;
        this.fullName = sourceUser.fullName;
        this.counts = sourceUser.counts;
    }
}
