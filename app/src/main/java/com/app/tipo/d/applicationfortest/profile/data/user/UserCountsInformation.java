package com.app.tipo.d.applicationfortest.profile.data.user;

import com.google.gson.annotations.SerializedName;

public class UserCountsInformation {
    @SerializedName("media")
    private int mediaCount;
    @SerializedName("followed_by")
    private int followedByCount;
    @SerializedName("follows")
    private int followersCount;

    public int getMediaCount() {
        return mediaCount;
    }

    public int getFollowedByCount() {
        return followedByCount;
    }

    public int getFollowersCount() {
        return followersCount;
    }
}
