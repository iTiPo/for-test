package com.app.tipo.d.applicationfortest.search;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;

import com.app.tipo.d.applicationfortest.R;
import com.app.tipo.d.applicationfortest.login.LoginActivity;
import com.app.tipo.d.applicationfortest.search.adapter.UsersSearchItem;
import com.app.tipo.d.applicationfortest.profile.ProfileActivity;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity implements SearchContract.View {
    @BindView(R.id.users_recycler_view)
    RecyclerView usersRecyclerView;
    @BindView(R.id.users_search_view)
    SearchView usersSearchView;

    private SearchContract.Presenter presenter;

    private FastItemAdapter<UsersSearchItem> fastItemAdapter;

    public static Intent createActivityIntent(Context packageContext) {
        return new Intent(packageContext, SearchActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setPresenter(new SearchPresenter(this));
        setupRecyclerView();
        setupSearchView();
    }

    private void setupRecyclerView() {
        setupAdapter();
        usersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        usersRecyclerView.setAdapter(fastItemAdapter);
    }

    private void setupAdapter() {
        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<UsersSearchItem>() {
            @Override
            public boolean onClick(View v, IAdapter<UsersSearchItem> adapter, UsersSearchItem item, int position) {
                startUserActivity();
                return true;
            }
        });
    }

    private void setupSearchView() {
        usersSearchView.onActionViewExpanded();
        usersSearchView.clearFocus();
        usersSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.searchUsers(newText);
                return false;
            }
        });
    }

    private void startUserActivity() {
        Intent intent = ProfileActivity.createActivityIntent(this);
        startActivity(intent);
    }

    @Override
    public void setPresenter(SearchContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showUsers(List<UsersSearchItem> users) {
        fastItemAdapter.set(users);
    }

    @Override
    public void startLoginActivity() {
        Intent intent = LoginActivity.createActivityIntent(getApplicationContext());
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.logout_button)
    public void onLogout(View view) {
        presenter.logout(this);
        startLoginActivity();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState = fastItemAdapter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        fastItemAdapter.withSavedInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }
}
