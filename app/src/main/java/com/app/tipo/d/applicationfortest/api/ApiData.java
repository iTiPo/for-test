package com.app.tipo.d.applicationfortest.api;

import java.io.InputStream;
import java.util.Properties;

public class ApiData {
    private static ApiData instance;

    private final String LOGIN_URL_FORMAT
            = "https://api.instagram.com/oauth/authorize/?client_id=%s&redirect_uri=%s&response_type=token";
    private final String REDIRECT_URI = "http://www.fakeredirecturifff.com";
    private final String CLIENT_ID = "0092c3d722a64b20903b616d50c23326";
    private final int SUCCESS_CODE = 200;

    private String accessToken;

    public static ApiData getInstance() {
        if (instance == null) {
            instance = new ApiData();
        }
        return instance;
    }

    private ApiData() {
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getLoginUrlFormat() {
        return LOGIN_URL_FORMAT;
    }

    public String getRedirectUri() {
        return REDIRECT_URI;
    }

    public String getClientId() {
        return CLIENT_ID;
    }

    public int getSuccessCode() {
        return SUCCESS_CODE;
    }
}
