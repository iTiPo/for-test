package com.app.tipo.d.applicationfortest.search.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.app.tipo.d.applicationfortest.R;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import java.util.List;

import butterknife.ButterKnife;

public class UsersSearchItem extends AbstractItem<UsersSearchItem, UsersSearchItem.ViewHolder> {
    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    private String userName;

    public UsersSearchItem(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public int getType() {
        return R.id.adapter_search_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.users_search_item;
    }

    @Override
    public void bindView(UsersSearchItem.ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.userName.setText(userName);
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.userName.setText(null);
    }

    @Override
    public ViewHolderFactory<? extends ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView userName;

        public ViewHolder(View view) {
            super(view);
            userName = ButterKnife.findById(view, R.id.user_name);
        }
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }
}
