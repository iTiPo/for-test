package com.app.tipo.d.applicationfortest.api;

import com.app.tipo.d.applicationfortest.post.data.PostsRequestData;
import com.app.tipo.d.applicationfortest.profile.data.user.UserRequestData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("v1/users/self/media/recent")
    Call<PostsRequestData> getItselfMedia(
            @Query("access_token") String accessToken,
            @Query("count") int count,
            @Query("max_id") String maxId);

    @GET("v1/users/self")
    Call<UserRequestData> getItself(
            @Query("access_token") String accessToken);
}
