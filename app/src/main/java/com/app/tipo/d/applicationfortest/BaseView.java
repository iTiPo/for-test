package com.app.tipo.d.applicationfortest;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
