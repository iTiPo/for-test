package com.app.tipo.d.applicationfortest.profile.data.user;

import com.google.gson.annotations.SerializedName;

public class RequestMeta {
    @SerializedName("code")
    private int code;

    public int getCode() {
        return code;
    }
}
