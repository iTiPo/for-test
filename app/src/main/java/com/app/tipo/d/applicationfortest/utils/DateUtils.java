package com.app.tipo.d.applicationfortest.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public static String getDate(int timeStamp) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date(timeStamp);

        return dateFormat.format(date);
    }
}
