package com.app.tipo.d.applicationfortest.post.data;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    @SerializedName("next_max_id")
    private String nextMaxId;

    public String getNextMaxId() {
        return nextMaxId;
    }
}
