package com.app.tipo.d.applicationfortest.profile;

import android.content.Context;

import com.app.tipo.d.applicationfortest.BasePresenter;
import com.app.tipo.d.applicationfortest.BaseView;
import com.app.tipo.d.applicationfortest.profile.adapter.PostItem;
import com.app.tipo.d.applicationfortest.post.data.Post;
import com.app.tipo.d.applicationfortest.profile.data.user.User;

import java.util.List;

public interface ProfileContract {
    interface View extends BaseView<Presenter> {
        void showPosts(List<Post> postItems);
    }

    interface Presenter extends BasePresenter {
        //void loadPosts();
        User getItselfUser();
        void loadPostsFirstPage(int count);
        void loadPostsNextPage(int count);
        List<PostItem> preparePostsItems(List<Post> posts, Context context);
    }
}
