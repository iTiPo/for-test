package com.app.tipo.d.applicationfortest.post.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Images implements Parcelable {
    public static final Creator<Images> CREATOR = new Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel source) {
            return new Images(source);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };

    @SerializedName("thumbnail")
    private ImageThumbnail thumbnail;

    @SerializedName("standard_resolution")
    private ImageStandard standard;

    public Images(Parcel source) {
        this.thumbnail = source.readParcelable(ImageThumbnail.class.getClassLoader());
        this.standard = source.readParcelable(ImageStandard.class.getClassLoader());
    }

    public ImageThumbnail getThumbnail() {
        return thumbnail;
    }

    public ImageStandard getStandard() {
        return standard;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(thumbnail, flags);
        dest.writeParcelable(standard, flags);
    }
}
