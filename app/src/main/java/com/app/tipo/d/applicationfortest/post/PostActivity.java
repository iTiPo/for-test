package com.app.tipo.d.applicationfortest.post;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.app.tipo.d.applicationfortest.R;
import com.app.tipo.d.applicationfortest.images.processing.BlurTransformation;
import com.app.tipo.d.applicationfortest.post.data.Caption;
import com.app.tipo.d.applicationfortest.post.data.Comments;
import com.app.tipo.d.applicationfortest.post.data.Likes;
import com.app.tipo.d.applicationfortest.post.data.Post;
import com.app.tipo.d.applicationfortest.utils.DateUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostActivity extends AppCompatActivity
        implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {
    private static final String EXTRA_POST = "com.app.tipo.d.applicationfortest.EXTRA_POST";
    private static final String POST_TYPE_IMAGE = "image";

    private Post post;
    private MediaPlayer mediaPlayer;
    private SurfaceHolder videoHolder;

    @BindView(R.id.post_image)
    ImageView postImage;
    @BindView(R.id.post_video)
    SurfaceView postVideo;
    @BindView(R.id.post_likes_count)
    TextView postLikesCount;
    @BindView(R.id.post_comments_count)
    TextView postCommentsCount;
    @BindView(R.id.post_creation_time)
    TextView postCreationTime;
    @BindView(R.id.post_caption)
    TextView postCaption;

    public static Intent createActivityIntent(Context packageContext, Post post) {
        Intent intent = new Intent(packageContext, PostActivity.class);
        intent.putExtra(EXTRA_POST, post);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        ButterKnife.bind(this);
        setupPost();
        setupPostInformation();
    }

    private void setupPost() {
        Intent intent = getIntent();
        if (intent != null) {
            post = intent.getParcelableExtra(EXTRA_POST);
        }
    }

    private void setupPostInformation() {
        if (post != null) {
            setupMedia();
            setupDetails();
            setupCaption();
        }
    }

    private void setupMedia() {
        if (post.getType().equals(POST_TYPE_IMAGE)) {
            loadImage();
        } else {
            loadVideo();
        }
    }

    private void loadImage() {
        postImage.setVisibility(View.VISIBLE);
        loadThumbnail();
    }

    private void loadThumbnail() {
        Picasso.with(this)
                .load(post.getImages().getThumbnail().getUrl())
                .transform(new BlurTransformation(this))
                .config(Bitmap.Config.RGB_565)
                .fit()
                .centerInside()
                .into(postImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        loadOriginal();
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    private void loadOriginal() {
        Picasso.with(this)
                .load(post.getImages().getStandard().getUrl())
                .noPlaceholder()
                .config(Bitmap.Config.RGB_565)
                .fit()
                .centerInside()
                .into(postImage);
    }

    private void loadVideo() {
        postVideo.setVisibility(View.VISIBLE);
        videoHolder = postVideo.getHolder();
        videoHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDisplay(videoHolder);
            mediaPlayer.setDataSource("https://scontent.cdninstagram.com/t50.2886-16/15343607_112755115880614_1980654491019509760_n.mp4");
            mediaPlayer.prepare();
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        } catch (Exception exc) {
            notifyVideoError();
        }
    }

    private void notifyVideoError() {
        Toast.makeText(this, "Ошибка при загрузке видео", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    private void setupDetails() {
        Likes likes = post.getLikes();
        Comments comments = post.getComments();

        if (likes != null) {
            setupTextViewValue(postLikesCount, String.valueOf(likes.getCount()));
        }
        if (comments != null) {
            setupTextViewValue(postCommentsCount, String.valueOf(comments.getCount()));
        }
        setupTextViewValue(postCreationTime, DateUtils.getDate(post.getCreationTime()));
    }

    private void setupCaption() {
        Caption caption = post.getCaption();
        if (caption != null) {
            setupTextViewValue(postCaption, post.getCaption().getText());
        }
    }

    private void setupTextViewValue(TextView textView, String value) {
        textView.setText(value);
    }
}
